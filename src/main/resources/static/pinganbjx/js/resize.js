var imgArr=["images/indexBgg.jpg","images/faqbg.jpg","images/backBtn.png","images/indexBtn.png","images/indexIcon.png","images/indexPeo.png","images/indexTit.png","images/shareTit.png","images/t1a.png","images/t1a1.png","images/t1b.png","images/t1b1.png","images/t1c.png","images/t1c1.png","images/t2a.png","images/t2a1.png","images/t2b.png","images/t2b1.png","images/t2c.png","images/t2c1.png","images/t3a.png","images/t3a1.png","images/t3b.png","images/t3b1.png","images/t3c.png","images/t3c1.png","images/t4a.png","images/t4a1.png","images/t4b.png","images/t4b1.png","images/t4c.png","images/t4c1.png","images/t5a.png","images/t5a1.png","images/t5b.png","images/t5b1.png","images/t5c.png","images/t5c1.png","images/t6a.png","images/t6a1.png","images/t6b.png","images/t6b1.png","images/t6c.png","images/t6c1.png","images/title1.png","images/title2.png","images/title3.png","images/title4.png","images/title5.png","images/title6.png","images/waitWxBtn.png","images/loading_preass.png","images/loading_preassbar.png","images/loadingIcon.png","images/loadingTit.png"];
function loading(arr, funLoading, funOnLoad, funOnError) {
  var numLoaded = 0,
    numError = 0,
    isObject = Object.prototype.toString.call(arr) === "[object Object]" ? true : false;

  var arr = isObject ? arr.get() : arr;
  for (i in arr) {
    var src = isObject ? $(arr[i]).attr("data-src") : arr[i];
    preload(src, arr[i]);
  }

  function preload(src, obj) {
    var img = new Image();
    img.onload = function() {
      numLoaded++;
      funLoading && funLoading(numLoaded, arr.length, src, obj);
      funOnLoad && numLoaded == arr.length && funOnLoad(numError);
    };
    img.onerror = function() {
      numLoaded++;
      numError++;
      funOnError && funOnError(numLoaded, arr.length, src, obj);
    };
    img.src = src;
  }

}
var funOnLoad = function(errors) {
     $('.loading').hide();
     $('.loading_top').removeClass('siderarrow');
};
var funloading = function(n, total, src, obj) {
  var i = parseInt(n / total * 100) + '%',
    num = parseInt(n / total * 100 * 1);
  $('.loading-plan').text(i);
  $("#progressbar" ).progressbar({
        value: num
    });
};

var funOnError = function(n, total, src, obj) {
  console.log("第 " + n + "张图片加载错！");
};
 loading(imgArr, funloading, funOnLoad, funOnError);



$(function(){

    //首页动画
    setTimeout(function(){
        $('.indexTit').css('display','block').addClass('animated bounceInDown');
         setTimeout(function(){
            $('.indexPeo').css('display','block').addClass('animated fadeIn');
            setTimeout(function(){
                $('.indexIcon01').css('display','block').addClass('animated bounceInUp');
                 $('.indexOpen,.indexTitFont').css('display','block').addClass('animated bounceInUp');
            }, 800)
        }, 1200)
    }, 300)
    //点击打开耳朵播放视频
    $('.indexOpen').on('click',function(){
        $('.indexTit').css('display','none').removeClass('animated bounceInDown');
        $('.indexPeo').css('display','none').removeClass('animated fadeIn');
        $('.indexIcon01').css('display','none').removeClass('animated bounceInUp');
        $('.indexOpen').css('display','none').removeClass('animated bounceInUp');
        // commented by wynston
        // $(".indexBg,.indexWrap").hide();
        // $(".video-wrap,#video-box").show();
        // $('#video-box')[0].play();
        //视频默认播放完成进入答题页面
        var audio = document.getElementById("video-box");
        audio.loop = false;
        makeVideoPlayableInline(audio);
        audio.controls=false;
        audio.play();
        audio.onplay = function () {
          audio.play();
        }
        audio.addEventListener("timeupdate",function() {
          // console.log(this.currentTime);
          if(this.currentTime>0.01){
        	  $(".indexBg,.indexWrap").hide();
              $(".video-wrap,#video-box").show().css('background', '#000');
          }
          if(this.currentTime>109){
              audio.pause();
              $('.question').show().css({"position":"fixed","top":"0","z-index":"999999"});
              $('#video-box,.video-wrap').remove().css({"position":"absolute","top":"-100%","right":"-100%"});
              $('.indexBg,.indexWrap').hide();
          }
        });
        audio.resume = function() {
            audio.play();
            $(".indexBg,.indexWrap").hide();
            $(".video-wrap,#video-box").show().css('background', '#000');
        };
        audio.addEventListener("loadedmetadata",function(){
           this.play();
        });
        audio.addEventListener('ended', function () {
              audio.controls=false;
              audio.pause();
              $('.question').show().css({"position":"fixed","top":"0","z-index":"999999"});
              $('#video-box,.video-wrap').remove().css({"position":"absolute","top":"-100%","right":"-100%"});
              $('.indexBg,.indexWrap').hide();

        }, false);
    })


      //回答问题
      initQuestion();
      var flag=false;
      $('.answer a').on('click',function(){
        var n =$(this).attr('class');
        $(this).find('img').attr("src","images/t"+random+n+"1.png");
        $('.pTit').show();
        setTimeout(function(){
          if(random == 1 && n=='b'){
            flag = true;
          }else if(random == 2 && n=='c'){
            flag = true;
          }else if(random == 3 && n=='b'){
            flag = true;
          }else if(random == 4 && n=='c'){
            flag = true;
          }else if(random == 5 && n=='a'){
            flag = true;
          }else if(random == 6 && n=='a'){
            flag = true;
          }

          if(flag){
            //跳转到抽奖页面
            //alert('正确');
          }else{
            //再次答题
            setTimeout(function(){
              initQuestion();
            }, 800);
          }
        },1000);

      });
})
var random;
function initQuestion(){
    random = parseInt(6 * Math.random());
  random = random+1;
  var titleHtml = '<img src="images/title'+random+'.png">',
  answerHtmla ='<img src="images/t'+random+'a.png">',
  answerHtmlb ='<img src="images/t'+random+'b.png">',
  answerHtmlc ='<img src="images/t'+random+'c.png">';
  $('.title').html(titleHtml);
  $('.answer .a').html(answerHtmla);
  $('.answer .b').html(answerHtmlb);
  $('.answer .c').html(answerHtmlc);
  $('.pTit').hide();
}

var ohtml = document.documentElement;
getSize();

window.onresize = function(){
    getSize();
}
function getSize(){

    var screenWidth = ohtml.clientWidth;
    if(screenWidth < 320){
        ohtml.style.fontSize = '20px';
    }else if(screenWidth > 750){
        ohtml.style.fontSize = '46.875px';
    }else{
        ohtml.style.fontSize = screenWidth/(640/40)+'px';
    }
}
