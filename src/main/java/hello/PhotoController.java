package hello;

import cloudinary.lib.PhotoUploadValidator;
import cloudinary.models.Photo;
import cloudinary.models.PhotoUpload;

import com.cloudinary.Cloudinary;
import com.cloudinary.StoredFile;
import com.cloudinary.utils.ObjectUtils;
import com.cloudinary.Singleton;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

@Controller
@RequestMapping("/album")
public class PhotoController {
	
	MongoClientURI uri = new MongoClientURI("mongodb://coolwan:whatcoolwan@do.houyewei.tk/?authSource=coolwan");
	private Jongo jongo = new Jongo(new MongoClient(uri).getDB("coolwan"));

    @Autowired
    private Cloudinary cloudinary;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String listPhotos(ModelMap model) {
    	MongoCollection images = jongo.getCollection("album");
    	MongoCursor<Photo> all = images.find().as(Photo.class);
        model.addAttribute("photos", all);
        return "photos";
    }

    @SuppressWarnings("rawtypes")
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String uploadPhoto(@ModelAttribute PhotoUpload photoUpload, BindingResult result, ModelMap model) throws IOException {
        PhotoUploadValidator validator = new PhotoUploadValidator();
        validator.validate(photoUpload, result);

        Map uploadResult = null;
        if (photoUpload.getFile() != null && !photoUpload.getFile().isEmpty()) {
            uploadResult = cloudinary.uploader().upload(photoUpload.getFile().getBytes(),
                    ObjectUtils.asMap("resource_type", "auto"));
            photoUpload.setPublicId((String) uploadResult.get("public_id"));
            Object version = uploadResult.get("version");
            if (version instanceof Integer) {
                photoUpload.setVersion(new Long((Integer) version));    
            } else {
                photoUpload.setVersion((Long) version);
            }
            
            photoUpload.setSignature((String) uploadResult.get("signature"));
            photoUpload.setFormat((String) uploadResult.get("format"));
            photoUpload.setResourceType((String) uploadResult.get("resource_type"));
        }

        if (result.hasErrors()){
            model.addAttribute("photoUpload", photoUpload);
            return "upload_form";
        } else {
            Photo photo = new Photo();
            photo.setTitle(photoUpload.getTitle());
            photo.setUpload(photoUpload);
            model.addAttribute("upload", uploadResult);
            MongoCollection imgs = jongo.getCollection("album");
            imgs.save(photo);
            model.addAttribute("photo", photo);
            return "upload";
        }
    }

    @RequestMapping(value = "/upload_form", method = RequestMethod.GET)
    public String uploadPhotoForm(ModelMap model) {
        model.addAttribute("photoUpload", new PhotoUpload());
        return "upload_form";
    }

    @RequestMapping(value = "/direct_upload_form", method = RequestMethod.GET)
    public String directUploadPhotoForm(ModelMap model) {
        model.addAttribute("photoUpload", new PhotoUpload());
        model.addAttribute("unsigned", false);
        return "direct_upload_form";
    }
    
    @SuppressWarnings("unchecked")
	@RequestMapping(value = "/direct_unsigned_upload_form", method = RequestMethod.GET)
    public String directUnsignedUploadPhotoForm(ModelMap model) throws Exception {
    	Map options = ObjectUtils.asMap("resource_type", "auto", "tags", "directly_uploaded", "callback", "/");
    	Map htmlOptions = ObjectUtils.asMap("alt", "sample");
    	String html = cloudinary.uploader().imageUploadTag("image_id", options, htmlOptions);
    	model.addAttribute("signature", html);
        return "direct_upload_form";
    }
}
