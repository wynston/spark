package hello;

import hello.model.Post;
import hello.repo.PostRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminController {
	
	@Autowired
	PostRepository repository;
	
	@ModelAttribute("post")
	public Post getPostObject() {
	  return new Post();
	}

	
	@RequestMapping("/")
	public String root(Model model) {
		model.addAttribute("hello", "酷玩--酷炫，有趣，好玩");
		return "index";
	}

	@RequestMapping("/index")
	public String index(Model model) {
		model.addAttribute("hello", "酷玩--酷炫，有趣，好玩");
		return "index";
	}
	
	@RequestMapping("/detail")
	public String detail(Model model) {
		model.addAttribute("hello", "Detail By 酷玩");
		return "detail";
	}
	
	@RequestMapping("/contact")
	public String contact(Model model) {
		model.addAttribute("hello", "联系我们");
		return "contact";
	}
	
	@RequestMapping("/about")
	public String about(Model model) {
		model.addAttribute("hello", "关于酷玩");
		return "about";
	}

	@RequestMapping("/user/index")
	public String userIndex(Model model) {
		model.addAttribute("Post", new Post());
		return "user/index";
	}
	
	@RequestMapping("/user/post/new")
	public String userPost(
	        final Post post, final BindingResult bindingResult, final ModelMap model) {
		// model.addAttribute("Post", new Post());
		if (bindingResult.hasErrors()) {
	        return "user/index";
	    }
	    // this.seedStarterService.add(seedStarter);
	    model.clear();
	    repository.save(post);
		System.out.println(model.toString());
		// model.addAttribute("Post", new Post());
		return "user/index";
	}
	
	
	@RequestMapping("/login")
	public String login() {
		return "login";
	}

	@RequestMapping("/login-error")
	public String loginError(Model model) {
		model.addAttribute("loginError", true);
		return "login";
	}

}
