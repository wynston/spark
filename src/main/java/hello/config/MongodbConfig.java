package hello.config;

import hello.repo.PersonRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoClientFactoryBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

import com.mongodb.DBAddress;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.WriteConcern;

@Configuration
@EnableMongoRepositories(basePackages = "hello.repo")
public class MongodbConfig extends AbstractMongoConfiguration  {
	
	//1.2.3.4
		@Value("${mongodb.url}")
		private String mongodbUrl;

		//hello
		@Value("${mongodb.db}")
		private String defaultDb;
		
		@Value("${mongodb.username}")
		private String  userName;

		@Value("${mongodb.password}")
		private String  password;

	  @Override
	  protected String getDatabaseName() {
	    return defaultDb;
	  }
	
	  @Override
	  public Mongo mongo() throws Exception {
		  
		  MongoClientURI uri = new MongoClientURI("mongodb://"+ userName + ":" + password + "@" + mongodbUrl + "/?authSource=" + defaultDb);
		  return new MongoClient(uri);
	  }

	  @Override
	  protected String getMappingBasePackage() {
	    return "com.oreilly.springdata.mongodb";
	  }


}