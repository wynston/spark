package hello;

import com.cloudinary.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.cloudinary.utils.ObjectUtils;



@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
public class Application {

	 @Bean
	 public Cloudinary cloudinary() {
	        return new Cloudinary(ObjectUtils.asMap(
	  	          "cloud_name", "deh8nvclk",
		          "api_key", "173213393725696",
		          "api_secret", "9mnopH_6ae3RUbemuGvMI9EQye4"));
	 }
	 
	 

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);    
    }

}
