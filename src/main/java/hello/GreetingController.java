package hello;


import java.util.HashMap;
import java.util.Map;

import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.jongo.MongoCursor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudinary.*;
import com.cloudinary.utils.ObjectUtils;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import hello.model.Friend;
import hello.model.Person;
import hello.model.Post;
import hello.repo.PersonRepository;

import org.springframework.beans.factory.annotation.Autowired;

@Controller
public class GreetingController {

	MongoClientURI uri = new MongoClientURI("mongodb://do.houyewei.tk");
	private Jongo jongo = new Jongo(new MongoClient(uri).getDB("py"));
	
	@Autowired
	private PersonRepository repository;
	
	@Autowired
	private Cloudinary cloudinary;
	
    @RequestMapping("/greeting")
    public String greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        //Map uploadResult = cloudinary.uploader().upload("http://www.example.com/image.jpg", ObjectUtils.emptyMap());
        System.out.println(repository.count());
        Person p = new Person(String.valueOf(System.currentTimeMillis()), "Hello", 14);
        repository.save(p);
        return "greeting";
    }

    @RequestMapping("/hahaha")
    public String hahaha(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
    	model.addAttribute("name", name);
       	MongoCollection friends = jongo.getCollection("img");
       	
    	MongoCursor<Friend> all = friends.find().skip(0).limit(60).as(Friend.class);
    	model.addAttribute("images", all);
    	String datestr = "img" + System.currentTimeMillis();
    	Map options = ObjectUtils.asMap("resource_type", "auto", "tags", "directly_uploaded", "callback", "/", "public_id", datestr);
    	Map htmlOptions = ObjectUtils.asMap("alt", "sample");
 
    	String html = cloudinary.uploader().imageUploadTag("image_upload", options, htmlOptions);
    	model.addAttribute("html", html);
    	
    	return "hahaha";
        // Map uploadResult = cloudinary.uploader().upload("http://www.example.com/image.jpg", ObjectUtils.emptyMap());
        // return all;
    }
    
    
    @RequestMapping("/validate")
    public @ResponseBody Map validate(@RequestParam(value="id", required=true) String id, Model model) {
    	
    	
    	// 'identifier' contains the value put in the hidden input field
    	// which includes the signature returned from Cloudinary.
    	StoredFile storedFile = new StoredFile();
    	storedFile.setPreloadedFile(id);
    	Map map = new HashMap();
    	if (!storedFile.getComputedSignature(cloudinary).equals(storedFile.getSignature())) {
    		map.put("success", false);
    		return map;// throw new Exception("Invalid upload signature");
    	} else {
    		map.put("success", true);
    		MongoCollection friends = jongo.getCollection("img");
    		String pubid = storedFile.getPublicId();
    		
    		Friend fri = new Friend();
    		String url = cloudinary.url()
    				  .transformation(new Transformation().width(400).height(300).crop("fill")).generate(pubid);
    		fri.setOu(url);
    		fri.setTu(url);
    		fri.setSt("图片上传服务");
    		friends.save(fri);
    		map.put("url", url);
    		// fri.setOu(storedFile);
    		return map;
    	  // Successful result
    	}
    }

    @RequestMapping("/haimages")
    public @ResponseBody MongoCursor<Friend> images(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
    	MongoCollection friends = jongo.getCollection("img");

    	MongoCursor<Friend> all = friends.find().as(Friend.class);
    	model.addAttribute("name", name);
        return all;
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/hahahaha") 
    public @ResponseBody MongoCursor<Post> hahahaha(@RequestParam(value="content", required=false, defaultValue="苟利国家生死以岂因祸福避趋之") String content, Model model) {
    	
    	Post post = new Post();
    	post.setInfo(content.replaceAll("(?s)<script.*?(/>|</script>)", ""));
    	post.setTimestamp(System.currentTimeMillis());
    	
    	MongoCollection posts = jongo.getCollection("posts");
    	posts.save(post);

    	MongoCursor<Post> all = posts.find().as(Post.class);
        return all;
    }
 
    @RequestMapping(method = RequestMethod.GET, value = "/hahahaha") 
    public @ResponseBody MongoCursor<Post> hahahahaha(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
    	
    	MongoCollection posts = jongo.getCollection("posts");
    	
    	MongoCursor<Post> all = posts.find().as(Post.class);
        return all;
    }

}
